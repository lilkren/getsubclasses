//
//  GetSubclass.m
//  GetSubclass
//
//  Created by Lil Kren on 12/30/15.
//  Copyright (c) 2015 Lil Kren. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GetSubclass.h"
#import <objc/runtime.h>

@implementation ClassUtils

NSDictionary* ClassGetVarNames(Class parentClass){
    NSMutableDictionary* ivarsDict=[NSMutableDictionary new];
    unsigned int count;
    Ivar* ivars=class_copyIvarList(parentClass, &count);
    for(int i=0; i<count; i++)
    {
        Ivar ivar= ivars[i];
        const char* name = ivar_getName(ivar);
        const char* typeEncoding = ivar_getTypeEncoding(ivar);
        [ivarsDict setObject: [NSString stringWithFormat: @"%s",typeEncoding] forKey: [NSString stringWithFormat: @"%s",name]];
    }
    free(ivars);
    return ivarsDict;
}

NSArray* ClassGetSubclasses(Class parentClass)
{
    int numClasses = objc_getClassList(NULL, 0);
    Class *classes = NULL;
    
    classes = (__unsafe_unretained Class *)malloc(sizeof(Class) * numClasses);
    numClasses = objc_getClassList(classes, numClasses);
    
    NSMutableArray *result = [NSMutableArray array];
    for (NSInteger i = 0; i < numClasses; i++)
    {
        Class superClass = classes[i];
        do
        {
            superClass = class_getSuperclass(superClass);
        } while(superClass && superClass != parentClass);
        
        if (superClass == nil)
        {
            continue;
        }
        
        [result addObject:classes[i]];
    }
    
    free(classes);
    
    return result;
}

void ClassLog(){
    printf("Awesome");
}


@end
