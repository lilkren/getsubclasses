//
//  GetSubclass.h
//  GetSubclass
//
//  Created by Lil Kren on 12/30/15.
//  Copyright (c) 2015 Lil Kren. All rights reserved.
//

//! Project version number for GetSubclass.
FOUNDATION_EXPORT double GetSubclassVersionNumber;

//! Project version string for GetSubclass.
FOUNDATION_EXPORT const unsigned char GetSubclassVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GetSubclass/PublicHeader.h>

#import <Foundation/Foundation.h>

@interface ClassUtils : NSObject

NSDictionary* ClassGetVarNames(Class parentClass);

NSArray* ClassGetSubclasses(Class parentClass);

void ClassLog();

@end

